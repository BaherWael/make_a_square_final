﻿namespace make_a_square_FINAL
{
    public enum PieceName
    {
        A = 'A',
        B = 'B',
        C = 'C',
        D = 'D',
        E = 'E'
    }

    class PieceNameMethods
    {
        static public PieceName getName(int index)
        {
            switch (index)
            {
                case 0:
                    return PieceName.A;
                case 1:
                    return PieceName.B;
                case 2:
                    return PieceName.C;
                case 3:
                    return PieceName.D;
                case 4:
                    return PieceName.E;
                default:
                    return 0;
            }
        }
    }
}