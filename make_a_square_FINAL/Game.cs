﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Win32;
using System.Windows;

namespace make_a_square_FINAL
{
    class Game
    {
        public const int EMPTY = 0;
        public const int FILLED = 1;
        private const int SOLUTION_BOARD_SIZE = 4;
        private List<Piece> originalPieces;
        private List<List<Piece>> allPiecesPossiblities;
        private List<Solution> solutionBoards;

        public Game()
        {
            originalPieces = new List<Piece>();
            allPiecesPossiblities = new List<List<Piece>>();
            solutionBoards = new List<Solution>();
        }

        public void Play()
        {
            ReadInputFile();
            if (!DoPiecesFitBoard())
            {
                Console.WriteLine("No Possible Solutions");
                return;
            }

            solutionBoards = FindSolutions();

            if (solutionBoards.Count == 0)
                Console.WriteLine("No Possible Solutions");
            else
                foreach (Solution solution in solutionBoards)
                    solution.PrintBoard();

            return;
        }

        private string BrowseFile()
        {
            // Configure open file dialog box
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.FileName = "Document"; // Default file name
            dlg.DefaultExt = ".txt"; // Default file extension
            dlg.Filter = "Text documents (.txt)|*.txt"; // Filter files by extension

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results
            if (result == true)
            {
                // Open document
                string filePath = dlg.FileName;
                return filePath;
            }

            return null;
        }

        private void ReadInputFile()
        {
            List<Piece> inputtedPieces = new List<Piece>();

            string filePath = this.BrowseFile();    //opens File dialog
            List<string> allLines = System.IO.File.ReadAllLines(@filePath).ToList();    // contains all lines read from txt file
            Func<string> dequeueLine = delegate ()
            {
                // dequeues first line of all lines
                string temp = allLines.First();
                allLines.RemoveAt(0);
                return temp;
            };

            int numOfPieces = Int32.Parse(dequeueLine());

            if (numOfPieces != 4 && numOfPieces != 5)
            {
                MessageBox.Show("Number of pieces should be 4 or 5 ", "Invalid Input File", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1);
            }
            else
            {

                for (int pieceNum = 0; pieceNum < numOfPieces; pieceNum++)
                {
                    String[] pieceDimensions = dequeueLine().Split(' ');
                    int pieceRows = Int32.Parse(pieceDimensions[0]);
                    int pieceCols = Int32.Parse(pieceDimensions[1]);
                    if(pieceRows > 4 || pieceRows < 1 || pieceCols > 4 || pieceCols < 1)
                    {
                        Console.WriteLine("Invalid Piece size");
                        Environment.Exit(1);
                    }
                    List<List<int>> pieceShape = new List<List<int>>();

                    for (var rowIndex = 0; rowIndex < pieceRows; rowIndex++)
                    {
                        pieceShape.Add(new List<int>());
                        foreach (char c in dequeueLine())
                        {
                            pieceShape[rowIndex].Add(Int32.Parse(c.ToString()));
                        }                      
                    }
                    originalPieces.Add( new Piece(PieceNameMethods.getName(pieceNum), pieceShape) );
                }
            }
        }

        private void FindAllPossibilities()
        {
            foreach(Piece p in originalPieces)
            {
                List<Piece> piecePossibilities = new List<Piece>();

                Piece rot90 = p.Rotate90Clockwise();
                Piece rot180 = p.Rotate180Clockwise();
                Piece rot270 = p.Rotate270Clockwise();

                piecePossibilities = AddAllPositions(p, piecePossibilities);
                piecePossibilities = AddAllPositions(rot90, piecePossibilities);
                piecePossibilities = AddAllPositions(rot180, piecePossibilities);
                piecePossibilities = AddAllPositions(rot270, piecePossibilities);
                piecePossibilities = AddAllPositions(p.FlipVertical(), piecePossibilities);
                piecePossibilities = AddAllPositions(rot90.FlipVertical(), piecePossibilities);
                piecePossibilities = AddAllPositions(rot180.FlipVertical(), piecePossibilities);
                piecePossibilities = AddAllPositions(rot270.FlipVertical(), piecePossibilities);

                allPiecesPossiblities.Add(piecePossibilities);
            }

            ////debugging
            //allPiecesPossiblities.ForEach(
            //p =>
            //{
            //    p.ForEach(s =>
            //    {
            //        s.PrintPieceShape();
            //    });
            //}
            //);
        }

        private List<Piece> AddAllPositions(Piece p, List<Piece> piecePossibilities)
        {
            int possibleRows = SOLUTION_BOARD_SIZE - p.getPieceRowsNumber();
            int possibleCols = SOLUTION_BOARD_SIZE - p.getPieceColumnsNumber();

            //loops all (x,y) possible positions
            for (int row_index = 0; row_index <= possibleRows; row_index++)
            {
                for (int col_index = 0; col_index <= possibleCols; col_index++)
                {
                    Piece newPiece = new Piece(p);  //VERY IMPORTANT ! TO CLONE OBJECT
                    newPiece.row_index = row_index;
                    newPiece.col_index = col_index;
                    piecePossibilities.Add(newPiece);
                }
            }
            return piecePossibilities;
        }

        private List<Solution> FindSolutions()
        {
            FindAllPossibilities();
            foreach (Piece piece1_possibility in allPiecesPossiblities[0])
            {
                foreach (Piece piece2_possibility in allPiecesPossiblities[1])
                {
                    foreach (Piece piece3_possibility in allPiecesPossiblities[2])
                    {
                        foreach (Piece piece4_possibility in allPiecesPossiblities[3])
                        {
                            if (allPiecesPossiblities.Count == 5)
                            {
                                foreach(Piece piece5_possibility in allPiecesPossiblities[4])
                                {
                                    Piece[] pieces = new Piece[] {piece1_possibility, piece2_possibility, piece3_possibility, piece4_possibility, piece5_possibility};
                                    solutionBoards = BuildSolution(pieces, solutionBoards);
                                    solutionBoards = RemoveDuplicateSolutions(solutionBoards);
                                }
                            }
                            else
                            {
                                Piece[] pieces = new Piece[] {piece1_possibility, piece2_possibility, piece3_possibility, piece4_possibility};
                                solutionBoards = BuildSolution(pieces, solutionBoards);
                                solutionBoards = RemoveDuplicateSolutions(solutionBoards);
                            }
                        }
                    }
                }
            }
            return solutionBoards;
        }

        private List<Solution> BuildSolution(Piece[] pieces, List<Solution> solutions)
        {
            Solution solution = new Solution();

            int pieceIndex = 0;
            foreach(Piece piece in pieces){
                int row_index = piece.row_index;
                for (int i = 0; i < piece.getPieceRowsNumber(); i++)
                {
                    int col_index = piece.col_index;
                    for (int j = 0; j < piece.getPieceColumnsNumber(); j++)
                    {
                        if (piece.GetPieceShape()[i][j] == 1 && solution.board[row_index, col_index] == EMPTY)
                            solution.board[row_index, col_index] = PieceNameMethods.getName(pieceIndex);
                        else if (piece.GetPieceShape()[i][j] == 0)
                        {
                            col_index++;
                            continue;
                        }
                        else if (piece.GetPieceShape()[i][j] == 1 && solution.board[row_index, col_index] != EMPTY)
                            return solutions;

                        col_index++;
                    }
                    row_index ++;
                }
                pieceIndex++;
            }
            if (CheckResult(solution))
                solutions.Add(solution);
            return solutions;
        }

        private bool CheckResult(Solution solution)
        {
            for (int row = 0; row < SOLUTION_BOARD_SIZE; row++)
            {
                for (int col = 0; col < SOLUTION_BOARD_SIZE; col++)
                {
                    if (solution.board[row, col] == EMPTY)
                    {
                        return false;
                    }
                }

            }
            return true;
        }

        private List<Solution> RemoveDuplicateSolutions(List<Solution> solutions)
        {
            bool noDuplicate = true;
            List<Solution> nonDuplicateSolutions = new List<Solution>();
            for(int i = 0; i < solutions.Count; i++)
            {
                for(int j = i+1; j < solutions.Count; j++)
                {
                    if(BoardsSimilar(solutions[i].board, solutions[j].board))
                    {
                        noDuplicate = false;
                        break;
                    }
                }
                if (noDuplicate)
                    nonDuplicateSolutions.Add(solutions[i]);
                noDuplicate = true;
            }
            return nonDuplicateSolutions;
        }

        private bool BoardsSimilar(PieceName[,] b1, PieceName[,] b2)
        {
            for(int i = 0; i < SOLUTION_BOARD_SIZE; i++)
            {
                for(int j = 0; j < SOLUTION_BOARD_SIZE; j++)
                {
                    if (b1[i, j] != b2[i, j])
                        return false;
                }
            }
            return true;
        }

        Boolean DoPiecesFitBoard()
        {
            int sum = 0;
            foreach (Piece piece in originalPieces)
                for (int i = 0; i < piece.getPieceRowsNumber(); i++)
                    for (int j = 0; j < piece.getPieceColumnsNumber(); j++)
                        sum += piece.GetPieceShape()[i][j];

            return (sum == SOLUTION_BOARD_SIZE * SOLUTION_BOARD_SIZE);
        }

        private class Solution
        {
            public PieceName[,] board;

            public Solution()
            {
                board = new PieceName[SOLUTION_BOARD_SIZE, SOLUTION_BOARD_SIZE];
            }

            public void PrintBoard()
            {
                for (int i = 0; i < SOLUTION_BOARD_SIZE; i++)
                {
                    for (int j = 0; j < SOLUTION_BOARD_SIZE; j++)
                    {
                        Console.Write(board[i, j]);
                    }
                    Console.Write(Environment.NewLine);
                }
                Console.Write(Environment.NewLine);
            }
        }
    }

}
