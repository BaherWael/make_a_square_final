﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace make_a_square_FINAL
{
                                        // USE Ctrl + F5 TO RUN PROJECT
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Game g = new Game();
            g.Play();

            //// Defining a single test piece
            //List<List<int>> testPieceShape = new List<List<int>>();
            //testPieceShape.Add( new List<int>() {1,0} );    //Row 1
            //testPieceShape.Add( new List<int>() {1,0} );    //Row 2
            //testPieceShape.Add( new List<int>() {1,1} );    //Row 3
            //Piece testPiece = new Piece(PieceName.A, testPieceShape);

            //testPiece.PrintPieceShape();

            ////testing rotate on single piece
            //Piece rotPiece = testPiece.Rotate180Clockwise();
            //rotPiece.PrintPieceShape();

            ////print original piece to make sure rotation didn't affect it
            //testPiece.PrintPieceShape();

        }
    }
}
