﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace make_a_square_FINAL
{
    class Piece
    {
        // Properties
        public int row_index;   //piece position
        public int col_index;   //piece position
        public PieceName name;
        private List<List<int>> pieceShape; //outer list is rows, inner list is columns

        // Constructors
        public Piece()
        {
            pieceShape = new List<List<int>>();
        }

        public Piece(PieceName name, List<List<int>> pieceShape)
        {
            this.name = name;
            this.pieceShape = pieceShape;
        }

        public Piece(Piece p)
        {
            this.row_index = p.row_index;
            this.col_index = p.col_index;
            this.name = p.name;
            this.pieceShape = p.ClonePieceShape();
        }

        // Public Methods
        public List<List<int>> GetPieceShape()
        {
            return this.pieceShape;
        }

        public int getPieceRowsNumber()
        {
            return this.pieceShape.Count;
        }

        public int getPieceColumnsNumber()
        {
            return this.pieceShape[0].Count;
        }

        public Piece FlipVertical()
        {
            List<List<int>> copy2d = this.ClonePieceShape();
            copy2d.Reverse();
            return new Piece(this.name, copy2d);
        }

        public Piece Rotate90Clockwise()
        {
            List<List<int>> pieceShapeCopy = ClonePieceShape();
            List<List<int>> pieceShapeCopyRotated = new List<List<int>>();

            for (int col = 0; col < this.getPieceColumnsNumber(); col++)
            {
                pieceShapeCopyRotated.Add(new List<int>());
                for(int row = this.getPieceRowsNumber()-1; row >= 0 ; row--)
                {
                    pieceShapeCopyRotated[col].Add(pieceShapeCopy[row][col]);
                }
            }

            return new Piece(this.name, pieceShapeCopyRotated);
        }

        public Piece Rotate180Clockwise()
        {
            List<List<int>> pieceShapeCopy = ClonePieceShape();
            List<List<int>> pieceShapeCopyRotated = new List<List<int>>();

            for (int row = 0; row < this.getPieceRowsNumber(); row++)
            {
                pieceShapeCopyRotated.Add(new List<int>());
                int roteted = getPieceRowsNumber() - row - 1;
                for (int col = this.getPieceColumnsNumber() - 1; col >= 0; col--)

                    pieceShapeCopyRotated[row].Add(pieceShapeCopy[roteted][col]);
            }

            return new Piece(this.name, pieceShapeCopyRotated);
        }

        public Piece Rotate270Clockwise()
        {
            List<List<int>> pieceShapeCopy = ClonePieceShape();
            List<List<int>> pieceShapeCopyRotated = new List<List<int>>();

            for (int col = 0; col < this.getPieceColumnsNumber(); col++)
            {
                //  Console.WriteLine("col"+ col);

                pieceShapeCopyRotated.Add(new List<int>());
                for (int row = 0; row < this.getPieceRowsNumber(); row++)
                {
                    //  Console.WriteLine("row" + row);
                    int roteted = this.getPieceColumnsNumber() - col - 1;
                    // Console.WriteLine("cast" + cast);

                    pieceShapeCopyRotated[col].Add(pieceShapeCopy[row][roteted]);
                }
            }

            return new Piece(this.name, pieceShapeCopyRotated);
        }

        public void PrintPieceShape()
        {
            String text = "";
            text += name + "\n";
            text += row_index + ", " + col_index + "\n";
            Console.WriteLine(text);
            pieceShape.ForEach((row) => { row.ForEach((col) => { Console.Write(col); }); Console.Write(Environment.NewLine); });
            Console.Write(Environment.NewLine);
        }

        private List<List<int>> ClonePieceShape()
        {
            return this.pieceShape.Select(a => a.ToList()).ToList();  //clone list
        }

    }
}
